import { Routes } from "@angular/router";
import { HomeComponent } from "./components/home/home.component";
import { RickmortyComponent } from "./components/rickmorty/rickmorty.component";

export const ROUTES: Routes=[
  {path: 'home', component: RickmortyComponent},
  {path:'', pathMatch:'full', redirectTo:'home'},
  {path:'**', pathMatch:'full', redirectTo:'home'},
];
