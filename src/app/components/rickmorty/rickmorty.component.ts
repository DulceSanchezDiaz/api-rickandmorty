import { Component, OnInit } from '@angular/core';
import { RickandmortyService } from 'src/app/services/rickandmorty.service';

@Component({
  selector: 'app-rickmorty',
  templateUrl: './rickmorty.component.html',
  styleUrls: ['./rickmorty.component.css']
})
export class RickmortyComponent implements OnInit {
//aqui se genera el arreglo personajes que se utilizara
public personajes: any[]=[];
//en el html para hacer las peticiones

  //le inyectamos el servicio de rickandmorty
  constructor( private _rickandmorty: RickandmortyService) { }

  ngOnInit(): void {
    //se manda a llamar a la funcion 
    this.getTodosPersonajes();
  }
  //funcion para obtener los personajes 
  getTodosPersonajes(){
    //mandamos a llamar a arreglo personajes
    this.personajes = [];
    

    this._rickandmorty.getPersonajes()
    .subscribe((data: any) => {
      console.log(data);

      this.personajes = data.results;
      //se entra al arreglo data, dentro de data entramos a results
      //donde se encuentran los datos que requerimos 

      //aqui se enivian a la consola los datos obtenidos 
      console.log('Personajes SETEADOS',this.personajes);
    })
  };

}
