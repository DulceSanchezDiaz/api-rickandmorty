import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';


@Injectable({
  providedIn: 'root'
})
export class RickandmortyService {
  //URL donde se hara la petición
  public URL= 'https://rickandmortyapi.com/api';

  //Inyectar el metodo HTTPCLIENT en el constructor
  constructor(private _http: HttpClient) { }

  //peticion get
  getPersonajes(){
    const uri = `${this.URL}/character`;

    return this._http.get(uri);
  }
}
